import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import NewCard from './NewCard';
import Cards from './Cards';
import { machine } from '../services/machine.js';

const styles = theme => ({
  root: {
    flexGrow: 1,
    margin: theme.spacing.unit * 2,

  },
  card: {
    padding: theme.spacing.unit,
    textAlign: 'left',
    color: theme.palette.text.secondary,
    elevation: 0,
  },
  textField: {
    marginTop: theme.spacing.unit - 19,
    maxWidth: 300,
  },
  title: {
    fontSize: 15,
    fontWeight: 400,
  },
  section: {
    marginTop: theme.spacing.unit * 5,
  },
  saveButton: {
    marginTop: theme.spacing.unit * 5,
  },
  button: {
    margin: theme.spacing.unit,
  },
  actions: {
    display: 'flex',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
    [theme.breakpoints.up('sm')]: {
      marginRight: -8,
    },
  },
});

class Board extends Component {
  constructor(props) {
    super(props);

    this.state = {
      cards: [],
      result: [],
    }

    this.handleSave = this.handleSave.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }

  _cardsLiteral() {
    let result = []
    this.state.cards.forEach(card => {
      result.push(card.name)
    });
    return '[' + result.join(', ') + ']';
  }

  handleBackspace(e) {
    if (e.which === 8 || e.which === 46) {
      this.setState({ result: '' });
    }
  }

  handleChange(e) {
    const regex = /[^wbso]/gi;
    const ch = String.fromCharCode(e.which);

    if (e.which === 8 || e.which === 46) {
      this.setState({ result: '' });
    }

    if (regex.test(ch)) {
      e.preventDefault();

      if(e.key === 'Enter' && e.target.value.length > 0 && this.state.cards.length > 0) {

        machine.cards = this._cardsLiteral();
        machine.build = e.target.value.toUpperCase();
        machine.dispatch('wonder');
        
        this.setState({ result: machine.result });
       
      }
    }
  }

  uuid() {
    return crypto.getRandomValues(new Uint32Array(4)).join('-');
  }
  
  handleSave(resources) {
    if (resources && resources.length > 0) {
      let resourceLiterals = [];

      resources.map(resource => {
        resourceLiterals.push(resource.label);
        return true;
      });

      this.setState({
        cards: [...this.state.cards, 
          { 
            name: resourceLiterals.join('/'), 
            resources: resources,
            id: this.uuid(), 
          }]
      });
    }
  }

  handleDelete(id) {
    this.setState(state => {
      const cards = [...state.cards];
      const cardToDelete = cards.indexOf(id);
      cards.splice(cardToDelete, 1);
      return { cards };
    });
  };

  render() {
    const { classes } = this.props;
    const { cards, result } = this.state;

    return (
      <div className={classes.root}>
        <Grid container spacing={24} alignItems="flex-start">
          <Grid item xs={12}>
            <Typography component="h2" variant="h2" gutterBottom>
              Can you build {<TextField
                id="wonder"
                className={classes.textField}
                defaultValue=""
                margin="normal"
                helperText="Allowed values: W, S, B, O"
                onKeyPress={(e) => { this.handleChange(e) }}
                onKeyDown={(e) => { this.handleBackspace(e) }}
          />}?
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography component="h4" variant="h4" color="textSecondary" gutterBottom>
              {result}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Card className={classes.card}>
              <CardContent>
                <NewCard onSave={this.handleSave}/>
              </CardContent>
            </Card>

          </Grid>
        </Grid>

        <Typography component="h3" variant="h5" gutterBottom align="left" className={classes.section}>
          Cards
        </Typography>

        <Cards cards={cards} onDelete={this.handleDelete}/>
      </div>
    );
  }
}

Board.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Board);
