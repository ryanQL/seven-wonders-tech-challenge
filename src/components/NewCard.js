import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Resources from './Resources';
import ResourcesArray from './ResourcesArray';
import classNames from 'classnames';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import { Divider } from '@material-ui/core';

const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    padding: theme.spacing.unit / 2,
    marginBottom: theme.spacing.unit * 2,
  },
  chip: {
    margin: theme.spacing.unit / 2,
  },
  label: {
    marginTop: theme.spacing.unit * 4,
    marginBottom: theme.spacing.unit * 2,
  },
  button: {
    marginTop: theme.spacing.unit * 3,
  },
  leftIcon: {
    marginRight: theme.spacing.unit,
  },
  rightIcon: {
    marginLeft: theme.spacing.unit,
  },
  iconSmall: {
    fontSize: 20,
  },
});

class NewCard extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      resources: [],
    }
    
    this.handleDelete = this.handleDelete.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.handleSave = this.handleSave.bind(this);

  }

  handleDelete(data) {
    this.setState(state => {
      const resources = [...state.resources];
      const chipToDelete = resources.indexOf(data);
      resources.splice(chipToDelete, 1);
      return { resources };
    });
  };

  handleAdd(resource) {
    let key = this.state.resources.length + 1;

    this.setState(state => {
      const resources = [...state.resources];
      resources.push({key: key, label: resource, matched: '_'});
      return { resources };
    });
  }

  handleSave() {
    this.props.onSave(this.state.resources);
    this.setState({
      resources: []
    })
  }
  
  render() {
    const { classes } = this.props;
    const { resources } = this.state;
    
    return (
      <div>
        <Typography 
          component="h5" 
          variant="headline" 
          align="left" 
          className={classes.label}>
          Available resources
        </Typography>

        <Resources 
          onAdd={ this.handleAdd } />

         <Typography 
          component="h5" 
          variant="headline" 
          align="left" 
          className={classes.label}
        >
          Card resources
        </Typography>       
        
        <ResourcesArray 
          resources={resources} 
          onDelete={this.handleDelete}
        />

        <Divider className={classes.button} />
      
        <Button variant="contained" color="primary" size="small" className={classes.button} onClick={this.handleSave}>
          <AddIcon className={classNames(classes.leftIcon, classes.iconSmall)} />
          Add Card
        </Button>
      
      </div>
    );
  }
}

NewCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NewCard);