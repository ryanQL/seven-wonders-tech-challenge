import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Chip from '@material-ui/core/Chip';
import AddIcon from '@material-ui/icons/Add';

const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'left',
    flexWrap: 'wrap',
    marginTop: theme.spacing.unit * 3,
  },
  chip: {
    marginRight: theme.spacing.unit,
    marginTop: theme.spacing.unit,
  },
});

class Resources extends React.Component {
  constructor(props){
    super(props);
    this.handleAddResource = this.handleAddResource.bind(this);
  }

  handleAddResource = (resource) => {
    this.props.onAdd(resource);
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Chip
          avatar={<Avatar>W</Avatar>}
          label="Wood"
          clickable
          className={classes.chip}
          color="primary"
          onDelete={(e) => this.handleAddResource('W')}
          deleteIcon={<AddIcon />}
        /> 
        <Chip
          avatar={<Avatar>B</Avatar>}
          label="Brick"
          clickable
          className={classes.chip}
          color="primary"
          onDelete={(e) => this.handleAddResource('B')}
          deleteIcon={<AddIcon />}
        /> 
        <Chip
          avatar={<Avatar>S</Avatar>}
          label="Stone"
          clickable
          className={classes.chip}
          color="primary"
          onDelete={(e) => this.handleAddResource('S')}
          deleteIcon={<AddIcon />}
        /> 
        <Chip
          avatar={<Avatar>O</Avatar>}
          label="Ore"
          clickable
          className={classes.chip}
          color="primary"
          onDelete={(e) => this.handleAddResource('O')}
          deleteIcon={<AddIcon />}
        />   
      </div>
    );
  }
}

Resources.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Resources);