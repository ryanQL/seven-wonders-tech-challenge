import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Chip from '@material-ui/core/Chip';
import { Typography } from '@material-ui/core';

const styles = theme => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    padding: theme.spacing.unit / 2,
  },
  chip: {
    margin: theme.spacing.unit / 2,
  },
});

class ResourcesArray extends React.Component {
  handleDelete(data){
    this.props.onDelete(data)
  };

  render() {
    const { classes, resources } = this.props;
    return (
      <div>
        {resources.length > 0 ? resources.map(resource => {
          let icon = null;

          return (
            <Chip
              key={resource.key}
              icon={icon}
              label={resource.label}
              onDelete={(e) => {this.handleDelete(e)}}
              className={classes.chip}
            />
          );
        }) : 
        <Typography variant="subtitle1">
          You can add resources by clicking on the available resources above
        </Typography> 
      }
      </div>
    );
  }
}

ResourcesArray.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ResourcesArray);