import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import CloseIcon from '@material-ui/icons/Close';

const styles = theme => ({
  card: {
    marginTop: theme.spacing.unit * 2,
    padding: theme.spacing.unit * 2,
    minWidth: 175,
  },
  title: {
    fontSize: 15,
    fontWeight: 400,
  },
});


class Cards extends Component {
  constructor(props){
    super(props);
    this.handleDelete = this.handleDelete.bind(this);
  }

  handleDelete(e, id) {
    this.props.onDelete(id);
  }

  render() {
    const { classes, cards } = this.props;

    const cardsArray = cards.map(card => (
      <Grid item xs={12} md={3} key={card.id}>
        <Card className={classes.card}>
          <CardHeader
            action={
              <IconButton onClick={(e) => { this.handleDelete(card.id) }}>
                <CloseIcon />
              </IconButton>
            }
            title={card.name}
          />
        </Card>
      </Grid>
    ));

    return (
      <Grid container spacing={24}>
        {cardsArray}
      </Grid>
    );
  }
};

Cards.propTypes = {
  classes: PropTypes.object.isRequired,
  cards: PropTypes.array.isRequired,
};

export default withStyles(styles)(Cards);