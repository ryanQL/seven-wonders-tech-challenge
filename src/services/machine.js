export const machine = {
  dispatch(actionName, ...payload) {
    const action = this.transitions[this.state][actionName];

    if (action) {
      console.log(`action dispatched: ${ actionName }`);
      action.apply(machine, payload);
    }
  },
  changeStateTo(newState) {
    console.log(`state changed: ${ newState }`);
    this.state = newState;
  },
  state: 'idle',
  cards: '',
  build: '',
  wonder: [],
  blanks: '_',
  ocurrences: {},
  result: '',
  transitions: {
    idle: {
      wonder: function() {
        this.wonder = Array.from(this.build);
        this.result = '';
        this.changeStateTo('running');
        this.dispatch('parse'); 
      },
      finish: function() {
        this.cards = '';
        this.build = '';
        this.ocurrences = {};
      }
    },
    running: {
      parse: function() {
        const regex = /\[(.*?)\]/;
        let matched = regex.exec(this.cards)[1];
        let data = matched.split(',').map((item) => {
          return item.trim();
        })
        this.dispatch('compute', data);       
      },   
      compute: function(data) {  
        this.ocurrences = this.wonder.reduce((acc, wonder) => { 
          if (wonder in acc) {
            acc[wonder]++;
          }
          else {
            acc[wonder] = 1;
          }
          return acc;
        }, {});
        this.dispatch('model', data);
      }, 
      model: function(data) {
        let model = [];
        data.forEach((card) => {
          model.push({ 
            card: card.split('/'), 
            processed: false, 
            matched: '_',
          });
        });
        this.changeStateTo('processing');
        this.dispatch('match', model);
      },
    },
    processing: {
      match: function(data) {
        let counter = Object.assign({}, this.ocurrences);

        data.forEach((entity) => {
          let resource = entity.card.find(w => { 
            if ((counter[w] - 1) < 0) {
              return false;
            } else {
              counter[w] -= 1;
              return this.wonder.includes(w);
            } 
          });

          entity.matched = resource;
          entity.processed = true;
        });

        this.dispatch('output', data);
      },
      output: function(data) {
        let counter = 0;
        let acc = '';
        
        data.forEach((entity) => {
          if (entity.matched) {
            counter++;
            acc += entity.matched;
          }
          else {
            entity.matched = '_';
          }
        });

        if (counter === this.build.length) {
          this.result = `YES ${acc}`;
        } else {
          this.result = 'NO';
        }

        this.changeStateTo('idle');
        this.dispatch('finish');
      }
    },
  }
}
