import {machine} from './machine';

describe('idle transition', () => {

  test('initial state is idle', () => {
    expect(machine.state).toBe('idle');
  });

  test('dispatch action wonder', () => {
    machine.cards = '[W/B/S/O, S/O]';
    machine.build = 'WS';
    machine.dispatch('wonder');
    expect(machine.wonder).toBeInstanceOf(Array);
  });

  test('dispatch action finish', () => {
    machine.dispatch('finish');
    expect(machine.cards).toBe('');
    expect(machine.build).toBe('');
    expect(machine.ocurrences).toBeInstanceOf(Object);
  });
});

describe('running transition', () => {
  beforeEach(() => {
    machine.cards = '[W/B/S/O, S/O]';
    machine.build = 'WS';
  })

  test('dispatch parse', () => {
    machine.changeStateTo('running');
    machine.dispatch('parse');
    expect(machine.state).toBe('idle');
  })

  test('dispatch compute', () => {
    machine.changeStateTo('running');
    machine.dispatch('compute', ["W/B/S/O"] );
    expect(machine.state).toBe('idle');
  })

  test('dispatch model', () => {
    machine.changeStateTo('running');
    machine.dispatch('model', ["W/B/S/O"]);
    expect(machine.state).toBe('idle');
  })
});

describe('processing transition', () => {
  beforeEach(() => {
    machine.cards = '[W/B/S/O, S/O]';
    machine.build = 'WS';
  })

  test('dispatch match', () => {
    machine.changeStateTo('running');
    machine.dispatch('match');
    expect(machine.state).toBe('running');
  })

  test('dispatch output', () => {
    machine.changeStateTo('running');
    machine.dispatch('output');
    expect(machine.state).toBe('running');
  })
});

describe('test cards examples', () => {
  const machineMock = jest.fn();

  test('Cards [W/B/S/O, W, S/B, S] Can you build WWSS', () => {
    machineMock.cards = "[W/B/S/O, W, S/B, S]"
    machineMock.cards = "WWSS"
    expect(machineMock.mockReturnValueOnce("YES WWSS"));
  })

  test('Cards [S, W/B/S/O, W, S/B] Can you build SWWB', () => {
    machineMock.cards = "[S, W/B/S/O, W, S/B]"
    machineMock.cards = "SWWB"
    expect(machineMock.mockReturnValueOnce("YES SWWB"));
  })

  test('Cards [B, S, W/B/O] can you build OS', () => {
    machineMock.cards = "[B, S, W/B/O]"
    machineMock.cards = "OS"
    expect(machineMock.mockReturnValueOnce("YES _SO"));
  })
  
  test('Cards [W/B/S/O, W/B/S/O] can you build WWW', () => {
    machineMock.cards = "[W/B/S/O, W/B/S/O]"
    machineMock.cards = "WWW"
    expect(machineMock.mockReturnValueOnce("NO"));
  })

  test('Cards [W/B/S/O, S/O, W/S, W/B, W/B, W, B] can you build WWBSSOO', () => {
    machineMock.cards = "[W/B/S/O, S/O, W/S, W/B, W/B, W, B]"
    machineMock.cards = "WWBSSOO"
    expect(machineMock.mockReturnValueOnce("NO"));
  })
})