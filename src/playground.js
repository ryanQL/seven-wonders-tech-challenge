const regex = /\[(.*?)\]/;
const strToMatch = "[S, W/B/S/O, W, S/B]";
let matched = regex.exec(strToMatch)[1];

let data = matched.split(',').map((item) => {
  return item.trim();
})

let model = [];
data.forEach((card) => {
  model.push({ card: card.split('/'), processed: false, result: '' } );
});

const wonder = Array.from('BSWW');

const match = (w) => {
  if (wonder.includes(w)) {
    return w
  } else {
    return false;
  }
}

model.forEach((entity) => {
  console.log(entity.card.find(match));
});

let counted = wonder.reduce(function (acc, wonder) { 
  if (wonder in acc) {
    acc[wonder]++;
  }
  else {
    acc[wonder] = 1;
  }
  return acc;
}, {});

console.log(counted);



